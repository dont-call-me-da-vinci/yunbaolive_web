<?php
/**
 * 首页
 */
class Api_Home extends PhalApi_Api {  

	public function getRules() {
		return array(  

            'getConfig' => array(
                'source'=>array('name' => 'source', 'type' => 'string','default'=>'app','desc' => '请求来源，app/wxmini'),
                'qiniu_sign' => array('name' => 'qiniu_sign', 'type' => 'string','desc' => '七牛sign'),
            ),

		
			'getHot' => array(
				'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
			),
			

			'getRecommend' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'min'=>1 ,'desc' => '用户ID'),
			),
			


            

		);
	}
	
    /**
     * 配置信息
     * @desc 用于获取配置信息
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return array info[0] 配置信息
     * @return object info[0].guide 引导页
	 * @return string info[0].guide.switch 开关，0关1开
	 * @return string info[0].guide.type 类型，0图片1视频
	 * @return string info[0].guide.time 图片时间
	 * @return array  info[0].guide.list
	 * @return string info[0].guide.list[].thumb 图片、视频链接
	 * @return string info[0].guide.list[].href 页面链接
     * @return string msg 提示信息
     */
    public function getConfig() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		
        $source=checkNull($this->source);
        $qiniu_sign=$this->qiniu_sign;
		
        $info = getConfigPub();
        
        unset($info['site_url']);
        unset($info['site_seo_title']);
        unset($info['site_seo_keywords']);
        unset($info['site_seo_description']);
        unset($info['site_icp']);
        unset($info['site_gwa']);
        unset($info['site_admin_email']);
        unset($info['site_analytics']);
        unset($info['copyright']);
        unset($info['qr_url']);
        unset($info['sina_icon']);
        unset($info['sina_title']);
        unset($info['sina_desc']);
        unset($info['sina_url']);
        unset($info['qq_icon']);
        unset($info['qq_title']);
        unset($info['qq_desc']);
        unset($info['qq_url']);
        unset($info['payment_des']);

        file_put_contents("qiniusign.txt", json_encode($qiniu_sign));
        
        $info_pri = getConfigPri();
        
        $list = getLiveClass();
    
        unset($info['voicelive_name']);
        unset($info['voicelive_icon']);
        
        $videoclasslist = getVideoClass();
        
        $level= getLevelList();
        
        foreach($level as $k=>$v){
            unset($v['level_up']);
            unset($v['addtime']);
            unset($v['id']);
            unset($v['levelname']);
            $level[$k]=$v;
        }
        
        $levelanchor= getLevelAnchorList();
        
        foreach($levelanchor as $k=>$v){
            unset($v['level_up']);
            unset($v['addtime']);
            unset($v['id']);
            unset($v['levelname']);
            $levelanchor[$k]=$v;
        }
        
        $info['liveclass']=$list;
        $info['videoclass']=$videoclasslist;
        
        $info['level']=$level;
        
        $info['levelanchor']=$levelanchor;
        
        $info['tximgfolder']='';//腾讯云图片存储目录
        $info['txvideofolder']='';//腾讯云视频存储目录
        $info['txcloud_appid']='';//腾讯云视频APPID
        $info['txcloud_region']='';//腾讯云视频地区
        $info['txcloud_bucket']='';//腾讯云视频存储桶
        $info['cloudtype']='1';//视频云存储类型
        
		$info['qiniu_domain']=DI()->config->get('app.Qiniu.space_host').'/';//七牛云存储空间地址
        $info['qiniu_uphost']=DI()->config->get('app.Qiniu.uphost');//七牛上传域名（小程序使用）
        $info['qiniu_region']=DI()->config->get('app.Qiniu.region');//七牛上存储区域（小程序使用）

        


        
        $info['guide']=[];
        



        $info['socket_url']=$info_pri['chatserver']; //socket url地址（小程序用）
        $info['qiniu_sign']=$qiniu_sign;
         
        $rs['info'][0] = $info;

        return $rs;
    }	

	
	
	
    /**
     * 获取热门主播
     * @desc 用于获取首页热门主播
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return array info[0]['slide'] 
     * @return string info[0]['slide'][].slide_pic 图片
     * @return string info[0]['slide'][].slide_url 链接
     * @return array info[0]['list'] 热门直播列表
     * @return string info[0]['list'][].uid 主播id
     * @return string info[0]['list'][].avatar 主播头像
     * @return string info[0]['list'][].avatar_thumb 头像缩略图
     * @return string info[0]['list'][].user_nicename 直播昵称
     * @return string info[0]['list'][].title 直播标题
     * @return string info[0]['list'][].city 主播位置
     * @return string info[0]['list'][].stream 流名
     * @return string info[0]['list'][].pull 播流地址
     * @return string info[0]['list'][].nums 人数
     * @return string info[0]['list'][].thumb 直播封面
     * @return string info[0]['list'][].level_anchor 主播等级
     * @return string info[0]['list'][].type 直播类型
     * @return string info[0]['list'][].goodnum 靓号
     * @return string msg 提示信息
     */
    public function getHot() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $p=checkNull($this->p);
        $domain = new Domain_Home();
		$key1='getSlide';
		$slide=getcaches($key1);
		if(!$slide){
			$where="status='1' and slide_id='2' ";
			$slide = $domain->getSlide($where);
			setcaches($key1,$slide);
		}
		
		
		//获取热门主播
		$key2="getHot_".$p;
		$list=getcaches($key2);
		if(!$list){
			$list = $domain->getHot($p);
			setcaches($key2,$list,2); 
		}
		
		/*获取推荐主播*/
		$key3="getRecommendLive_1";
		$recommend_list=getcaches($key3);
		if(!$recommend_list){
			$recommend_list = $domain->getRecommendLive(1);
			setcaches($key3,$recommend_list,2); 
		}

        $rs['info'][0]['slide'] = $slide;
        $rs['info'][0]['list'] = $list;
        $rs['info'][0]['recommend'] = $recommend_list;

        return $rs;
    }
	

	/**
     * 推荐主播
     * @desc 用于显示推荐主播
     * @return int code 操作码，0表示成功
     * @return array info 会员列表
     * @return string info[].id 用户ID
     * @return string info[].user_nicename 用户昵称
     * @return string info[].avatar 头像
     * @return string info[].fans 粉丝数
     * @return string info[].isattention 是否关注，0未关注，1已关注
     * @return string msg 提示信息
     */
    public function getRecommend() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=checkNull($this->uid);
		
		$key='getRecommend';
		$info=getcaches($key);
		if(!$info){
			$domain = new Domain_Home();
			$info = $domain->getRecommend();

			setcaches($key,$info,60*10);
		}
		
		foreach($info as $k=>$v){
			$info[$k]['isattention']=(string)isAttention($uid,$v['id']);
		}

        $rs['info'] = $info;

        return $rs;
    }	


} 
